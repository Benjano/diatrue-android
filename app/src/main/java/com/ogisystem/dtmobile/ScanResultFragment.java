package com.ogisystem.dtmobile;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import mehdi.sakout.fancybuttons.FancyButton;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by Nir Benjano on 1/6/2017.
 */

public class ScanResultFragment extends Fragment {


    private static final String TAG = ScanResultFragment.class.getSimpleName();

    Bitmap mScanBitmap;
    ImageView mScanImageView;
    FancyButton mSave;
    PhotoViewAttacher mAttacher;


    public static ScanResultFragment getInstance() {

        return new ScanResultFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_scan_result, container, false);
        mScanImageView = (ImageView) view.findViewById(R.id.scan_result);
        mScanImageView.setImageBitmap(mScanBitmap);

        // Attach a PhotoViewAttacher, which takes care of all of the zooming functionality.
        // (not needed unless you are going to change the drawable later)
        mAttacher = new PhotoViewAttacher(mScanImageView);


        mSave = (FancyButton) view.findViewById(R.id.save);
        mSave.setOnClickListener(new View.OnClickListener() {
            boolean saved = false;

            @Override
            public void onClick(View view) {
                if (!saved) {
                    new AsyncTask<Object, Object, Object>() {
                        @Override
                        protected Object doInBackground(Object... objects) {
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd_MM_yyyy_HH:mm:ss");
                            fixMediaDir();
                            MediaStore.Images.Media.insertImage(getContext().getContentResolver(), mScanBitmap, "Daitrue_" + simpleDateFormat.format(new Date()) +".png", "Diatrue scan result");

                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getContext(), getResources().getString(R.string.saved), Toast.LENGTH_LONG).show();
                                }
                            });
                            return null;
                        }
                    }.execute();
                    saved = true;
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.already_saved), Toast.LENGTH_LONG).show();
                }

            }
        });

        return view;
    }

    private void fixMediaDir() {
        File sdcard = Environment.getExternalStorageDirectory();
        if (sdcard != null) {
            File mediaDir = new File(sdcard, "DCIM/Camera");
            if (!mediaDir.exists()) {
                mediaDir.mkdirs();
            }
        }
    }


    @Override
    public void onStop() {
        super.onStop();
//        mScanBitmap.recycle();
//        mScanBitmap = null;
    }

    public void setScan(Bitmap scan) {
        if (mScanBitmap != null) {
            mScanBitmap.recycle();
            mScanBitmap = null;
        }
        mScanBitmap = scan;
    }
}
