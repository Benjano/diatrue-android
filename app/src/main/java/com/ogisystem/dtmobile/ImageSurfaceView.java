package com.ogisystem.dtmobile;

import android.hardware.Camera;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.ogisystem.dtmobile.utils.SharedPreferencesUtil;

import java.io.IOException;
import java.util.List;

public class ImageSurfaceView extends SurfaceView implements SurfaceHolder.Callback {

    private static final String TAG = ImageSurfaceView.class.getSimpleName();

    private Camera mCamera;
    private SurfaceHolder surfaceHolder;

    List<Camera.Size> mSupportedPreviewSizes;
    Camera.Size mPreviewSize;

    public ImageSurfaceView(Context context) {
        super(context);
        init();
    }

    public ImageSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ImageSurfaceView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    private void init() {
        mCamera = checkDeviceCamera();

        this.surfaceHolder = getHolder();
        this.surfaceHolder.addCallback(this);

        mSupportedPreviewSizes = mCamera.getParameters().getSupportedPreviewSizes();

    }

//    exposure-compensation-step
//    "max-exposure-compensation" -> "6"
//    "min-exposure-compensation" -> "-6"


    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            this.mCamera.setPreviewDisplay(holder);
            this.mCamera.startPreview();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.e(TAG, "surfaceChanged => w=" + width + ", h=" + height);
        // If your preview can change or rotate, take care of those events here.
        // Make sure to stop the preview before resizing or reformatting it.
        if (getHolder().getSurface() == null) {
            // preview surface does not exist
            return;
        }

        // stop preview before making changes
        try {
            mCamera.stopPreview();
        } catch (Exception e) {
            // ignore: tried to stop a non-existent preview
        }

        // set preview size and make any resize, rotate or reformatting changes here
        // start preview with new settings
        try {
            mCamera.setDisplayOrientation(90);
            mCamera.setPreviewDisplay(getHolder());
            mCamera.startPreview();

            Camera.Parameters parameters = mCamera.getParameters();
            parameters.setExposureCompensation(SharedPreferencesUtil.read(getContext(), SharedPreferencesUtil.Type.EXPOSURE));
            parameters.setExposureCompensation(parameters.getMaxExposureCompensation());
            parameters.setAutoExposureLock(false);

            parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            int iso = SharedPreferencesUtil.read(getContext(), SharedPreferencesUtil.Type.ISO);
            if (iso < 100) {
                parameters.set("iso", "auto");
            } else {
                parameters.set("iso", iso);
            }
            mCamera.setParameters(parameters);

        } catch (Exception e) {
            Log.d(TAG, "Error starting camera preview: " + e.getMessage());
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (mCamera != null) {
            this.mCamera.stopPreview();
            mCamera.setPreviewCallback(null);
            getHolder().removeCallback(this);
            mCamera.release();
        }
    }

    private Camera checkDeviceCamera() {
        Camera mCamera = null;
        try {
            releaseCameraAndPreview();
            mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
        } catch (Exception e) {
            Log.e(TAG, "failed to open Camera");
            e.printStackTrace();
        }
        return mCamera;
    }

    public void takePicture(Camera.PictureCallback pictureCallback) {
        mCamera.takePicture(null, null, pictureCallback);
    }

    public void flashlight(boolean on) {
        Camera.Parameters parameters = mCamera.getParameters();
        parameters.setFlashMode(on ? Camera.Parameters.FLASH_MODE_TORCH : Camera.Parameters.FLASH_MODE_OFF);
        mCamera.setParameters(parameters);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int width = resolveSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        final int height = resolveSize(getSuggestedMinimumHeight(), heightMeasureSpec);

        if (mSupportedPreviewSizes != null) {
            mPreviewSize = getOptimalPreviewSize(mSupportedPreviewSizes, width, height);
        }

        float ratio;
        if (mPreviewSize.height >= mPreviewSize.width)
            ratio = (float) mPreviewSize.height / (float) mPreviewSize.width;
        else
            ratio = (float) mPreviewSize.width / (float) mPreviewSize.height;

        // One of these methods should be used, second method squishes preview slightly
        setMeasuredDimension(width, (int) (width * ratio));
//        setMeasuredDimension((int) (width * ratio), height);
    }

    private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio = (double) h / w;

        if (sizes == null)
            return null;

        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;

        for (Camera.Size size : sizes) {
            double ratio = (double) size.height / size.width;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
                continue;

            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }

        return optimalSize;
    }

    public void releaseCameraAndPreview() {
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }
    }

    public void resume() {
        if (mCamera == null) {
            init();
        }
    }
}