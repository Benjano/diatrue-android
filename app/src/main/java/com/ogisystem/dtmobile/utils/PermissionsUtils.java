package com.ogisystem.dtmobile.utils;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

/**
 * Created by Nir Benjano on 1/6/2017.
 */

public class PermissionsUtils {


    public static boolean hasCameraPermissions(Activity activity) {
        return ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED;
    }

    public static boolean hasStoragePermissions(Activity activity) {
        return ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED;
    }

    public static void checkCameraPermissions(Activity activity) {
        if (hasCameraPermissions(activity)) {
            //ask for authorisation
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA}, 50);
        }
    }

    public static void checkStoragePermissions(Activity activity) {
        if (hasStoragePermissions(activity)) {
            //ask for authorisation
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 50);
        }
    }
}
