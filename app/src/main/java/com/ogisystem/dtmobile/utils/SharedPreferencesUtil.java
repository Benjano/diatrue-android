package com.ogisystem.dtmobile.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Nir Benjano on 1/6/2017.
 */

public class SharedPreferencesUtil {


    private static final String PREFERENCE_NAME = "DT_Preferences";
    public static final String KEY_EXPOSURE_TIME = "Key_exposureTime";
    public static final String KEY_MIN_EXPOSURE_TIME = "Key_min_exposureTime";
    public static final String KEY_MAX_EXPOSURE_TIME = "Key_max_exposureTime";
    public static final String KEY_EXPOSURE = "Key_exposure-compensation";
    public static final String KEY_MIN_EXPOSURE = "Key_min_exposure-compensation";
    public static final String KEY_MAX_EXPOSURE = "Key_max_exposure-compensation";
    public static final String KEY_ISO = "Key_iso";
    public static final String KEY_MIN_ISO = "Key_min_iso";
    public static final String KEY_MAX_ISO = "Key_max_iso";


    public static void write(Context context, Type type, int value) {
        SharedPreferences sharedPref = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(type.key, value);
        editor.commit();
    }

    public static int read(Context context, Type type) {
        SharedPreferences sharedPref = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        return sharedPref.getInt(type.key, type.defValue);
    }

    public static int readMin(Context context, Type type) {
        SharedPreferences sharedPref = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        return sharedPref.getInt(type.KeyMinValue, type.defMin);
    }

    public static int readMax(Context context, Type type) {
        SharedPreferences sharedPref = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        return sharedPref.getInt(type.KeyMaxValue, type.defMax);
    }


    public enum Type {
        EXPOSURE_TIME(KEY_EXPOSURE_TIME, KEY_MIN_EXPOSURE_TIME, KEY_MAX_EXPOSURE_TIME, 5000, 50000, 5000),
        EXPOSURE(KEY_EXPOSURE, KEY_MIN_EXPOSURE, KEY_MAX_EXPOSURE, 0, 2, -2),
        ISO(KEY_ISO, KEY_MIN_ISO, KEY_MAX_ISO, 0, 1600, 0);

        final String key, KeyMaxValue, KeyMinValue;
        final int defValue, defMax, defMin;

        Type(String key, String KeyMaxValue, String KeyMinValue, int defValue, int defMax, int defMin) {
            this.key = key;
            this.KeyMaxValue = KeyMaxValue;
            this.KeyMinValue = KeyMinValue;
            this.defValue = defValue;
            this.defMax = defMax;
            this.defMin = defMin;
        }
    }
}
