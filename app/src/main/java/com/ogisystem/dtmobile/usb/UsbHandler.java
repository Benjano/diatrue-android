package com.ogisystem.dtmobile.usb;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by Nir Benjano on 1/6/2017.
 */

public class UsbHandler extends Handler {

    private static final String TAG = UsbHandler.class.getSimpleName();

    @Override
    public void handleMessage(Message msg) {
        Log.d(TAG, msg.toString());
        super.handleMessage(msg);
    }
}
