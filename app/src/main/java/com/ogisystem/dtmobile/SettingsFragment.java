package com.ogisystem.dtmobile;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import com.akexorcist.roundcornerprogressbar.IconRoundCornerProgressBar;
import com.devspark.robototextview.widget.RobotoTextView;
import com.ogisystem.dtmobile.utils.SharedPreferencesUtil;
import com.ogisystem.dtmobile.views.SettingCardView;

/**
 * Created by Nir Benjano on 1/6/2017.
 */

public class SettingsFragment extends Fragment {


    private static final String TAG = SettingsFragment.class.getSimpleName();

    ImageSurfaceView mImageSurfaceView;
    IconRoundCornerProgressBar mCounterProgress;
    RobotoTextView mCounterTime;

    public static SettingsFragment getInstance() {
        return new SettingsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        SettingCardView exposureTime = (SettingCardView) view.findViewById(R.id.settings_exposure_time);
        exposureTime.setType(SharedPreferencesUtil.Type.EXPOSURE_TIME);

        SettingCardView exposure = (SettingCardView) view.findViewById(R.id.settings_exposure);
        exposure.setType(SharedPreferencesUtil.Type.EXPOSURE);

        SettingCardView iso = (SettingCardView) view.findViewById(R.id.settings_iso);
        iso.setType(SharedPreferencesUtil.Type.ISO);

        return view;
    }
}
