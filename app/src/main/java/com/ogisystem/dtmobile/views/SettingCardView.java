package com.ogisystem.dtmobile.views;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.SeekBar;

import com.devspark.robototextview.widget.RobotoTextView;
import com.ogisystem.dtmobile.R;
import com.ogisystem.dtmobile.utils.SharedPreferencesUtil;
import com.ogisystem.dtmobile.utils.SharedPreferencesUtil.Type;

/**
 * Created by Nir Benjano on 1/6/2017.
 */

public class SettingCardView extends CardView {


    private Type mType;

    private SeekBar mSeekBar;
    private RobotoTextView mTitle, mValue;

    private int mMax, mMin, mChosenValue;

    public SettingCardView(Context context) {
        super(context);
        init(context);
    }

    public SettingCardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public SettingCardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater.from(context).inflate(R.layout.view_setting_card, this);
        setMaxCardElevation(getResources().getDimension(R.dimen.card_max_elevation));
        setRadius(getResources().getDimension(R.dimen.card_radius));

        mSeekBar = (SeekBar) findViewById(R.id.seekbar);
        mTitle = (RobotoTextView) findViewById(R.id.title);
        mValue = (RobotoTextView) findViewById(R.id.value);

        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                String presentValue = "";
                switch (mType) {
                    case EXPOSURE_TIME: {
                        mChosenValue = mMin + (mSeekBar.getProgress() / 1000) * 1000;
                        presentValue = mChosenValue / 1000 + "";
                        break;
                    }
                    case EXPOSURE: {
                        mChosenValue = mMin + mSeekBar.getProgress();
                        presentValue = mChosenValue + "";
                        break;
                    }

                    case ISO: {
                        mChosenValue = mMin + mSeekBar.getProgress();
                        if (mChosenValue < 100) {
                            presentValue = "auto";
                        } else {
                            presentValue = (mMin + seekBar.getProgress()) + "";
                        }
                        break;
                    }
                }
                mValue.setText(presentValue);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }


    public void setType(Type type) {
        mType = type;
        int lastValue = 0;
        switch (type) {
            case EXPOSURE_TIME:
                mTitle.setText(R.string.settings_exposure_time_title);
                mMin = SharedPreferencesUtil.readMin(getContext(), Type.EXPOSURE_TIME);
                mMax = SharedPreferencesUtil.readMax(getContext(), Type.EXPOSURE_TIME);
                lastValue = SharedPreferencesUtil.read(getContext(), Type.EXPOSURE_TIME);
                break;
            case EXPOSURE:
                mTitle.setText(R.string.settings_exposure_compensation);
                mMin = SharedPreferencesUtil.readMin(getContext(), Type.EXPOSURE);
                mMax = SharedPreferencesUtil.readMax(getContext(), Type.EXPOSURE);
                lastValue = SharedPreferencesUtil.read(getContext(), Type.EXPOSURE);
                break;
            case ISO:
                mTitle.setText(R.string.settings_iso);
                mMin = SharedPreferencesUtil.readMin(getContext(), Type.ISO);
                mMax = SharedPreferencesUtil.readMax(getContext(), Type.ISO);
                lastValue = SharedPreferencesUtil.read(getContext(), Type.ISO);
                break;

        }
        mSeekBar.setMax(mMax - mMin);
        mSeekBar.setProgress(lastValue - mMin);
    }


    @Override
    protected void onDetachedFromWindow() {
        SharedPreferencesUtil.write(getContext(), mType, mChosenValue);
        super.onDetachedFromWindow();
    }
}
