package com.ogisystem.dtmobile;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.devspark.robototextview.widget.RobotoTextView;
import com.ogisystem.dtmobile.usb.Demo;
import com.ogisystem.dtmobile.usb.DemoCustomHID;
import com.ogisystem.dtmobile.usb.UsbHandler;
import com.ogisystem.dtmobile.utils.PermissionsUtils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import mehdi.sakout.fancybuttons.FancyButton;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class MainActivity extends AppCompatActivity {

    private static final String ACTION_USB_PERMISSION = "com.ogisystem.dtmobile.USB_PERMISSION";
    private static final String TAG = MainActivity.class.getSimpleName();


    Demo mDemo = null;
    PendingIntent pendingIntent = null, mPermissionIntent;

    private Mode mMode;
    CameraFragment mCameraFragment;
    ScanResultFragment mScanResultFragment;

    private FancyButton mScan, mLed;
    private SwitchCompat mSwitchFlash;
    private RobotoTextView mFlashTitle;
    private RelativeLayout mControllers;
    FrameLayout mFragmentContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mCameraFragment = CameraFragment.getInstance();
        mScanResultFragment = ScanResultFragment.getInstance();

        PermissionsUtils.checkCameraPermissions(this);
        PermissionsUtils.checkStoragePermissions(this);

        mPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);
        IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
        registerReceiver(receiver, filter);

        mLed = (FancyButton) findViewById(R.id.switch_led);
        mLed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mDemo != null) {
                    synchronized (mDemo) {
                        if (mDemo != null) {
                            mDemo.toggleLEDs();
                        }
                    }
                }
            }
        });

//        mLedTitle = (RobotoTextView) findViewById(R.id.led_title);
//        mLedTitle.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mSwitchLed.setChecked(mSwitchLed.isChecked());
//            }
//        });

        mSwitchFlash = (SwitchCompat) findViewById(R.id.switch_flash);
        mSwitchFlash.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                mCameraFragment.flashlight(b);
            }
        });

        mFlashTitle = (RobotoTextView) findViewById(R.id.flash_title);
        mFlashTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSwitchFlash.setChecked(mSwitchFlash.isChecked());
            }
        });


        mScan = (FancyButton) findViewById(R.id.scan);
        mScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mMode == Mode.CAMERA) {

                    if (getSupportActionBar() != null) {
                        getSupportActionBar().hide();
                    }
                    mControllers.setVisibility(View.GONE);

                    mCameraFragment.setDemo(mDemo);
                    mCameraFragment.scan(new Camera.PictureCallback() {
                        @Override
                        public void onPictureTaken(final byte[] data, Camera camera) {
                            new AsyncTask<Object, Object, Object>() {
                                @Override
                                protected Object doInBackground(Object... objects) {
                                    Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                                    Matrix matrix = new Matrix();
                                    matrix.postRotate(90);
                                    Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                                    bitmap.recycle();
                                    mScanResultFragment.setScan(rotatedBitmap);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            switchMode(Mode.SCAN_RESULT);
                                        }
                                    });
                                    return null;
                                }
                            }.execute();
                        }
                    });
                } else {
                    switchMode(Mode.CAMERA);
                }
            }
        });

        mFragmentContainer = (FrameLayout) findViewById(R.id.fragment_container);
        mControllers = (RelativeLayout) findViewById(R.id.fullscreen_content_controls);

        switchMode(Mode.CAMERA);

    }


    private void switchMode(Mode mode) {
        mSwitchFlash.setChecked(false);

        Fragment fragment = null;
        switch (mode) {
            case SETTINGS:
                fragment = SettingsFragment.getInstance();
                if (getSupportActionBar() != null) {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                }

                mControllers.setVisibility(View.GONE);
                mMode = Mode.SETTINGS;
                break;
            case CAMERA:
                fragment = mCameraFragment;
                mScan.setText(getResources().getString(R.string.scan));
                if (getSupportActionBar() != null) {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                }

                mControllers.setVisibility(View.VISIBLE);
                mMode = Mode.CAMERA;
                break;
            case SCAN_RESULT:
                fragment = mScanResultFragment;
                if (getSupportActionBar() != null) {
                    getSupportActionBar().show();
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                }

                mScan.setText(getResources().getString(R.string.camera));
                mMode = Mode.SCAN_RESULT;
                break;
        }

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        ft.setCustomAnimations(R.anim.fade_in,
                R.anim.fade_out);
        ft.replace(R.id.fragment_container, fragment);
        ft.commit();

    }


    @Override
    public void onResume() {
        super.onResume();
        // Check to see if it was a USB device attach that caused the app to
        // start or if the user opened the program manually.
        Intent intent = getIntent();
        String action = intent.getAction();
        if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
            UsbDevice device = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
            mDemo = loadDemo(device);
        } else {
            UsbManager manager = (UsbManager) getSystemService(Context.USB_SERVICE);
            HashMap<String, UsbDevice> deviceList = manager.getDeviceList();
            Iterator<UsbDevice> deviceIterator = deviceList.values().iterator();

            while (deviceIterator.hasNext()) {
                mDemo = loadDemo(deviceIterator.next());
                if (mDemo != null) {
                    Toast.makeText(this, R.string.led_connected, Toast.LENGTH_SHORT).show();
                    break;
                }
            }
        }
        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        registerReceiver(receiver, filter);
        pendingIntent = PendingIntent.getBroadcast(this, 0, new Intent(getPackageName() + ".USB_PERMISSION"), 0);
    }

    @Override
    public void onPause() {
        if (mDemo != null) {
            mDemo.close();
        }
        mDemo = null;
        unregisterReceiver(receiver);
        super.onPause();
    }

    public Demo loadDemo(UsbDevice device) {
        Demo tempDemo = Demo.loadDemo(this.getApplicationContext(), device, new UsbHandler());
        if (tempDemo != null) {
            if (tempDemo.getClass().equals(DemoCustomHID.class)) {
                mLed.setEnabled(true);
            }
        }
        return tempDemo;
    }

    BroadcastReceiver receiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
                UsbDevice device = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                if (device != null) {
                    synchronized (mDemo) {
                        if (mDemo != null) {
                            mDemo.close();
                            mDemo = null;
                        }
                    }
                }
                mLed.setEnabled(false);
                Toast.makeText(context, R.string.led_disconnected, Toast.LENGTH_SHORT).show();
            }
        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_settings: {
                switchMode(Mode.SETTINGS);
                return true;
            }

            case android.R.id.home: {
                onBackPressed();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mMode != Mode.CAMERA) {
            switchMode(Mode.CAMERA);
        } else {
            super.onBackPressed();
        }
    }

    enum Mode {
        SETTINGS, CAMERA, SCAN_RESULT;
    }
}
