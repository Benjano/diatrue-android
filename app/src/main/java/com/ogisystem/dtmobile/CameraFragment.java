package com.ogisystem.dtmobile;

import android.hardware.Camera;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.StringDef;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.akexorcist.roundcornerprogressbar.IconRoundCornerProgressBar;
import com.devspark.robototextview.widget.RobotoTextView;
import com.ogisystem.dtmobile.usb.Demo;
import com.ogisystem.dtmobile.utils.SharedPreferencesUtil;

/**
 * Created by Nir Benjano on 1/6/2017.
 */

public class CameraFragment extends Fragment {


    private static final String TAG = CameraFragment.class.getSimpleName();

    ImageSurfaceView mImageSurfaceView;
    IconRoundCornerProgressBar mCounterProgress;
    RobotoTextView mCounterTime;
    CountDownTimer mCountDownTimer;

    private Demo mDemo = null;

    private int mCountDownTime;

    public static CameraFragment getInstance() {
        return new CameraFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_camera, container, false);
        mImageSurfaceView = (ImageSurfaceView) view.findViewById(R.id.camera_surface);
        mCounterProgress = (IconRoundCornerProgressBar) view.findViewById(R.id.counter_progress);
        mCounterTime = (RobotoTextView) view.findViewById(R.id.counter_time);


        mCounterTime.setVisibility(View.INVISIBLE);
        mCounterProgress.setVisibility(View.INVISIBLE);

        return view;
    }

    public void scan(final Camera.PictureCallback pictureCallback) {
        mCounterTime.setVisibility(View.VISIBLE);
        mCounterProgress.setVisibility(View.VISIBLE);

        final String counterTextMany = getResources().getString(R.string.counter_time_many);
        final String counterTextSingle = getResources().getString(R.string.counter_time_single);
        final String capturing = getResources().getString(R.string.capturing);
        mCountDownTime = SharedPreferencesUtil.read(getContext(), SharedPreferencesUtil.Type.EXPOSURE_TIME);

        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
        }

        mCountDownTimer = new CountDownTimer(SharedPreferencesUtil.read(getContext(), SharedPreferencesUtil.Type.EXPOSURE_TIME) - 100, 20) {
            public void onTick(long millisUntilFinished) {
                mCounterProgress.setProgress((mCountDownTime - millisUntilFinished) * 100 / mCountDownTime);
                Log.d(TAG, "Progress: " + (mCountDownTime - millisUntilFinished) * 100 / mCountDownTime);
                Log.d(TAG, "Millies to Finish: " + millisUntilFinished);


                int secondsToFinish = (int) (millisUntilFinished / 1000) + 1;

                mCounterTime.setText(String.format(secondsToFinish > 1 ? counterTextMany : counterTextSingle, secondsToFinish + ""));
            }

            public void onFinish() {
                Log.d(TAG, "Progress: 100");
                mCounterProgress.setProgress(100);
                mCounterTime.setText(capturing);
                if (mDemo != null) {
                    if (mDemo.isOn()) {
                        mDemo.toggleLEDs();
                    }
                    mDemo = null;
                }
                mImageSurfaceView.takePicture(pictureCallback);
            }
        }.start();
    }

    public void flashlight(boolean on) {
        mImageSurfaceView.flashlight(on);
    }

    public void setDemo(Demo demo) {
        mDemo = demo;
    }

    @Override
    public void onPause() {
        mImageSurfaceView.releaseCameraAndPreview();
        super.onPause();
    }

    @Override
    public void onResume() {
        mImageSurfaceView.resume();
        super.onResume();
    }

    @Override
    public void onStop() {
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
        }
        super.onStop();
    }
}
